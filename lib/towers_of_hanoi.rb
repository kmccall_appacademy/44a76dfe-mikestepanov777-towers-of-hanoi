class TowersOfHanoi
  attr_reader :towers
  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from_idx, to_idx)
    @towers[to_idx].push(@towers[from_idx].pop)
  end

  def valid_move?(from_idx, to_idx)
    return false if @towers[from_idx].empty?
    unless @towers[to_idx].empty?
      return false if @towers[from_idx].last > @towers[to_idx].last
    end
    true
  end

  def won?
    @towers[1] == [3, 2, 1] || @towers[2] == [3, 2, 1]
  end

  def render
    greet
    while won? == false
      puts "Tower #1: #{@towers[0]} : Tower #2: #{@towers[1]} : Tower #3: #{@towers[2]}"
      puts "Select tower to move disk from:"
      from_idx = gets.chomp.to_i - 1
      puts "Select tower to move this disk:"
      to_idx = gets.chomp.to_i - 1
      if valid_move?(from_idx, to_idx)
        move(from_idx, to_idx)
      else
        puts "----------INVALID MOVE----------"
      end
    end
    puts "You have won the \"Towers of Hanoi\" game!"
    sleep(2)
    puts "Victory for Sylvanas!"
  end

  def greet
    puts "---Welcome to \"Towers of Hanoi\" game!---"
    puts "Do you want to play?"
    puts "1: Yes!"
    puts "2: No."
    input = gets.chomp.to_i
    if input == 2
      puts "You don't want to play."
      puts "Sad."
      exit
    end
  end

end

#play = TowersOfHanoi.new
#play.render
